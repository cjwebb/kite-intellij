# Kite-IntelliJ

The IntelliJ plugin for Kite.

## Todo
- Figure out how we register a new menu option
- Connect to server, and print incoming messages to the console.
- Translate messages into the IntelliJ filesystem wrapper.

- Top-level menu to connect/disconnect project. Connect pops up a dialog to supply the (autofilled) topic name.

## Later
- Investigate writing this in Kotlin.
