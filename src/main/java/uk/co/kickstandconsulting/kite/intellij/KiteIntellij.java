package uk.co.kickstandconsulting.kite.intellij;

import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.components.ApplicationComponent;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.Buffer;

public class KiteIntellij implements ApplicationComponent {

    @NotNull public String getComponentName() {
        return "Kite";
    }

    private final int port = 12345;
    private Socket socket;

    public void initComponent() {
//        try {
//// todo - don't block here
//            socket = new ServerSocket(port).accept();
//            BufferedReader reader = readerOf(socket);
//            String message = reader.readLine();
//            System.out.println(message);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    private BufferedReader readerOf(Socket socket) throws IOException {
        return new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public void disposeComponent() {
        // clean up connections
    }
}
