package uk.co.kickstandconsulting.kite.intellij;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;

public class TextBoxes extends AnAction {
    @Override
    public void actionPerformed(AnActionEvent e) {
        Project project = e.getProject();
        String txt = Messages.showInputDialog(project, "Hello", "Input name", Messages.getQuestionIcon());
        Messages.showMessageDialog(project, "Hello, " + txt, "Information", Messages.getInformationIcon());
    }
}
